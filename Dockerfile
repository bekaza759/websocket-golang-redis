FROM golang:1.12 AS builder
COPY . /src
WORKDIR /src

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /server .

FROM alpine
COPY --from=builder ./server ./
COPY ./public ./public
EXPOSE 8080

ENTRYPOINT ["./server"]