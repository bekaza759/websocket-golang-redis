[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

# Go Websocket Example: Chat

This is a simple application that serves tasty WebSockets to your users
with Go + Redis (used so that the app can scale beyond 1 dyno).

Check out the [live demo](http://go-websocket-chat-demo.herokuapp.com) or [read the docs](https://devcenter.heroku.com/articles/go-websockets).

## Deploy to cloud run

```bash
gcloud beta run deploy chatservice --source=. \
     --vpc-connector=[VPC_CONNECTOR_NAME] \
     --set-env-vars REDIS_URL=redis://[REDIS_IP]:6379 \
     --max-instances=1000 \
     --concurrency=250 \
     --timeout=3600 \
     --allow-unauthenticated
```
